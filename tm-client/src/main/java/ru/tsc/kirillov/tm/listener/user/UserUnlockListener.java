package ru.tsc.kirillov.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.kirillov.tm.dto.request.UserUnlockRequest;
import ru.tsc.kirillov.tm.enumerated.Role;
import ru.tsc.kirillov.tm.event.ConsoleEvent;
import ru.tsc.kirillov.tm.util.TerminalUtil;

@Component
public final class UserUnlockListener extends AbstractUserListener {

    @NotNull
    @Override
    public String getName() {
        return "user-unlock";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Разблокировка пользователя.";
    }

    @Override
    @EventListener(condition = "@userUnlockListener.getName() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[Разблокировка пользователя]");
        System.out.println("Введите логин пользователя:");
        @NotNull final String login = TerminalUtil.nextLine();
        getUserEndpoint().unlockUser(new UserUnlockRequest(getToken(), login));
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[] { Role.ADMIN };
    }

}
