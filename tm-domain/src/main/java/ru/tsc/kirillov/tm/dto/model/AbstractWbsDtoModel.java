package ru.tsc.kirillov.tm.dto.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import ru.tsc.kirillov.tm.api.model.IWBS;
import ru.tsc.kirillov.tm.enumerated.Status;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@MappedSuperclass
@NoArgsConstructor
@EntityListeners(AuditingEntityListener.class)
public abstract class AbstractWbsDtoModel extends AbstractUserOwnedDtoModel implements IWBS {

    @NotNull
    @Column(nullable = false)
    private String name = "";

    @Column
    @Nullable
    private String description = "";

    @NotNull
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @NotNull
    @Column(nullable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING,
            pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
    private Date created = new Date();

    @Nullable
    @Column(name = "date_begin")
    @JsonFormat(shape = JsonFormat.Shape.STRING,
            pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
    private Date dateBegin;

    @Nullable
    @Column(name = "date_end")
    @JsonFormat(shape = JsonFormat.Shape.STRING,
            pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
    private Date dateEnd;

    public AbstractWbsDtoModel(
            @NotNull final String userId,
            @NotNull final String name
    ) {
        super(userId);
        this.name = name;
    }

    public AbstractWbsDtoModel(
            @NotNull final String userId,
            @NotNull final String name,
            @Nullable final String description
    ) {
        super(userId);
        this.name = name;
        this.description = description;
    }

    public AbstractWbsDtoModel(
            @NotNull final String name,
            @NotNull final Status status,
            @Nullable final Date dateBegin
    ) {
        super();
        this.name = name;
        this.status = status;
        this.dateBegin = dateBegin;
    }

    public AbstractWbsDtoModel(
            @NotNull final String userId,
            @NotNull final String name,
            @Nullable final String description,
            @Nullable final Date dateBegin,
            @Nullable final Date dateEnd
    ) {
        super(userId);
        this.name = name;
        this.description = description;
        this.dateBegin = dateBegin;
        this.dateEnd = dateEnd;
    }

    @Override
    public String toString() {
        return name + " : " + getStatus().getDisplayName() + " : " + description;
    }

}
