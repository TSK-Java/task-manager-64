package ru.tsc.kirillov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.tsc.kirillov.tm.model.Project;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

@Repository
public class ProjectRepository {

    @NotNull
    private final Map<String, Project> projects = new LinkedHashMap<>();

    {
        add(new Project("Проект 1"));
        add(new Project("Проект 2"));
        add(new Project("Проект 3"));
        add(new Project("Проект 4"));
    }

    public void create() {
        add(new Project(("Новый проект: " + System.currentTimeMillis())));
    }

    public void add(@NotNull final Project project) {
        projects.put(project.getId(), project);
    }

    public void save(@NotNull final Project project) {
        projects.replace(project.getId(), project);
    }

    @Nullable
    public Collection<Project> findAll() {
        return projects.values();
    }

    @Nullable
    public Project findById(@NotNull final String id) {
        return projects.get(id);
    }

    public void removeById(@NotNull final String id) {
        projects.remove(id);
    }

}
