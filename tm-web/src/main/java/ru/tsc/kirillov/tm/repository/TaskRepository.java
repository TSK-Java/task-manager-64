package ru.tsc.kirillov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.tsc.kirillov.tm.model.Task;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

@Repository
public class TaskRepository {

    @NotNull
    private final Map<String, Task> tasks = new LinkedHashMap<>();

    {
        add(new Task("Задача 1"));
        add(new Task("Задача 2"));
        add(new Task("Задача 3"));
        add(new Task("Задача 4"));
    }

    public void create() {
        add(new Task(("Новая задача: " + System.currentTimeMillis())));
    }

    public void add(@NotNull final Task task) {
        tasks.put(task.getId(), task);
    }

    public void save(@NotNull final Task task) {
        tasks.replace(task.getId(), task);
    }

    @Nullable
    public Collection<Task> findAll() {
        return tasks.values();
    }

    @Nullable
    public Task findById(@NotNull final String id) {
        return tasks.get(id);
    }

    public void removeById(@NotNull final String id) {
        tasks.remove(id);
    }

}
