package ru.tsc.kirillov.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.tsc.kirillov.tm.enumerated.Status;
import ru.tsc.kirillov.tm.model.Project;
import ru.tsc.kirillov.tm.repository.ProjectRepository;

import java.util.Collection;

@Controller
public class ProjectController {

    @NotNull
    @Autowired
    private ProjectRepository projectRepository;

    @Nullable
    private Collection<Project> getProjects() {
        return projectRepository.findAll();
    }

    @NotNull
    private Status[] getStatuses() {
        return Status.values();
    }

    @NotNull
    @GetMapping("/project/create")
    public String create() {
        projectRepository.create();
        return "redirect:/projects";
    }

    @NotNull
    @GetMapping("/project/delete/{id}")
    public String delete(@NotNull @PathVariable("id") final String id) {
        projectRepository.removeById(id);
        return "redirect:/projects";
    }

    @NotNull
    @PostMapping("/project/edit/{id}")
    public String edit(
            @NotNull @ModelAttribute("project") final Project project,
            @NotNull final BindingResult result
    ) {
        projectRepository.save(project);
        return "redirect:/projects";
    }

    @NotNull
    @GetMapping("/project/edit/{id}")
    public ModelAndView edit(@NotNull @PathVariable("id") final String id) {
        @NotNull final Project project = projectRepository.findById(id);
        @NotNull final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("project-edit");
        modelAndView.addObject("project", project);
        modelAndView.addObject("statuses", getStatuses());
        return modelAndView;
    }

    @NotNull
    @GetMapping("/projects")
    public ModelAndView list() {
        return new ModelAndView("project-list", "projects", getProjects());
    }

}
