package ru.tsc.kirillov.tm.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.format.annotation.DateTimeFormat;
import ru.tsc.kirillov.tm.enumerated.Status;
import ru.tsc.kirillov.tm.util.DateUtil;

import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Task {

    @NotNull
    private String id = UUID.randomUUID().toString();

    @NotNull
    private String name = "";

    @Nullable
    private String description = "";

    @NotNull
    private Status status = Status.NOT_STARTED;

    @Nullable
    @DateTimeFormat(pattern = DateUtil.PATTERN)
    private Date dateBegin;

    @Nullable
    @DateTimeFormat(pattern = DateUtil.PATTERN)
    private Date dateEnd;

    @Nullable
    private String projectId = null;

    public Task(@NotNull final String name) {
        this.name = name;
    }

}
