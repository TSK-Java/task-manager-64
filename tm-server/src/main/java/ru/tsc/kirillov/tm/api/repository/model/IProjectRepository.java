package ru.tsc.kirillov.tm.api.repository.model;

import ru.tsc.kirillov.tm.model.Project;

public interface IProjectRepository extends IWbsRepository<Project> {
}
